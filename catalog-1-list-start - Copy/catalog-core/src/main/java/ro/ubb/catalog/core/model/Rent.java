package ro.ubb.catalog.core.model;


import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "rent")
@IdClass(RentPK.class)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
//@EqualsAndHashCode()
@Builder
public class Rent implements Serializable {

    @Id
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "movie_id")
    private Movie movie;

    @Id
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "client_id")
    private Client client;

    @Column(name = "dueDate")
    private String dueDate;

    @Override
    public String toString() {
        return "Rent{" +
                "movieId=" + movie.getId() +
                ", clientId=" + client.getId() +
                ", dueDate='" + dueDate + '\'' +
                '}' + super.toString();
    }
}
