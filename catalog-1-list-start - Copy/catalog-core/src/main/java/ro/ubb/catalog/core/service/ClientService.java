package ro.ubb.catalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.core.repository.ClientRepository;


import java.util.List;

@Service
public class ClientService implements IClientService{
    public static final Logger log = LoggerFactory.getLogger(ClientService.class);

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public List<Client> getAllClients() {
        log.trace("getAllClients(ClientService) --- method entered");
        List<Client> result = clientRepository.findAll();
        log.trace("getAllClients(ClientService): result={}", result);
        return result;
    }

    @Override
    public Client saveClient(Client client) {
        log.trace("saveClient(ClientService) - method entered: client={}", client);
        Client result = clientRepository.save(client);
        log.trace("saveClient(ClientService) - method finished");
        return result;
    }

    @Override
    public Client updateClient(Long id, Client client) {
        log.trace("updateClient(ClientService) - method entered: client={}", client);
        Client update = clientRepository.findById(id).orElse(client);
        update.setCnp(client.getCnp());
        update.setName(client.getName());
        log.trace("updateClient(ClientService) - method finished");
        return update;
    }

    @Override
    public void deleteById(Long id) {
        log.trace("deleteById(ClientService) - method entered: id={}",id);
        clientRepository.deleteById(id);
        log.trace("deleteById(ClientService) - method finished");
    }
}
