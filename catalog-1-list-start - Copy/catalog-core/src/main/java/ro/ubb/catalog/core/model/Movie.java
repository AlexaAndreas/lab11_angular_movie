package ro.ubb.catalog.core.model;

import lombok.*;

import javax.persistence.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;


@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
//@EqualsAndHashCode(callSuper = true)
@Builder
public class Movie extends BaseEntity<Long> {

    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "genre", nullable = false)
    private String genre;


    @OneToMany(mappedBy = "movie", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Rent> rents = new HashSet<>();

    public Set<Client> getClients() {
        rents = rents == null ? new HashSet<>() :
                rents;
        return Collections.unmodifiableSet(
                this.rents.stream().
                        map(Rent::getClient).
                        collect(Collectors.toSet()));
    }


    public void addClient(Client client) {
        Rent rent = new Rent();
        rent.setClient(client);
        rent.setMovie(this);
        rents.add(rent);
    }

    public void addClient(Set<Client> clients) {
        clients.forEach(this::addClient);
    }

    public void addDueDate(Client client, String dueDate) {
        Rent rent = new Rent();
        rent.setClient(client);
        rent.setDueDate(dueDate);
        rent.setMovie(this);
        rents.add(rent);
    }

    @Override
    public String toString() {
        return "Movie{" +
                "name='" + name + '\'' +
                ", genre='" + genre + '\'' +
                '}' + super.toString();
    }
}
