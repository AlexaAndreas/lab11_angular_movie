package ro.ubb.catalog.core.model;


import lombok.*;

import javax.persistence.*;
import java.text.CollationElementIterator;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;


@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
//@EqualsAndHashCode(callSuper = true)
@Builder
public class Client extends BaseEntity<Long> {
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "cnp", nullable = false)
    private String cnp;

    @OneToMany(mappedBy = "client", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Rent> rents = new HashSet<>();

    public Set<Movie> getMovies(){
        return Collections.unmodifiableSet(
                rents.stream()
                .map(Rent::getMovie)
                .collect(Collectors.toSet())
        );
    }

    public void addMovie(Movie movie) {
        Rent rent = new Rent();
        rent.setMovie(movie);
        rent.setClient(this);
        rents.add(rent);
    }

    public void addRental(Movie movie, String dueDate) {
        Rent rent = new Rent();
        rent.setMovie(movie);
        rent.setDueDate(dueDate);
        rent.setClient(this);
        rents.add(rent);
    }

    @Override
    public String toString() {
        return "Client{" +
                "cnp='" + cnp + '\'' +
                ", name='" + name + '\'' +
                '}' + super.toString();
    }
}
