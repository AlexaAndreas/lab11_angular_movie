package ro.ubb.catalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ro.ubb.catalog.core.model.Movie;
import ro.ubb.catalog.core.model.Rent;
import ro.ubb.catalog.core.service.IMovieService;
import ro.ubb.catalog.core.service.MovieService;
import ro.ubb.catalog.web.converter.RentConverter;
import ro.ubb.catalog.web.dto.MovieDto;
import ro.ubb.catalog.web.dto.RentDto;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
public class RentalsController {

    public static final Logger log = LoggerFactory.getLogger(MovieController.class);

    @Autowired
    private IMovieService movieService;

    @Autowired
    private RentConverter rentConverter;


    @RequestMapping(value = "/rents/{movieId}", method = RequestMethod.GET)
    public Set<RentDto> getClientMovies(
            @PathVariable final Long movieId) {
        return new HashSet<>(rentConverter.convertModelsToDtos(movieService.findMovie(movieId).getRents()));
    }
}
