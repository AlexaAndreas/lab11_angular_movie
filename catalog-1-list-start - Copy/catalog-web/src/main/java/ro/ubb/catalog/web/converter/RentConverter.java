package ro.ubb.catalog.web.converter;

import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.core.model.Rent;
import ro.ubb.catalog.web.dto.RentDto;

@Component
public class RentConverter extends AbstractConverter<Rent,RentDto>{
    @Override
    public Rent convertDtoToModel(RentDto dto) {
        throw new RuntimeException("not yet implemented!");
    }

    @Override
    public RentDto convertModelToDto(Rent rent) {
        return RentDto.builder()
                .clientId(rent.getClient().getId())
                .movieId(rent.getMovie().getId())
                .dueDate(rent.getDueDate())
                .build();
    }
}
