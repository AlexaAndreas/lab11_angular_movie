package ro.ubb.catalog.web.dto;

import lombok.*;




@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MovieDto extends BaseDto{
    private String name;
    private String genre;

    @Override
    public String toString() {
        return "MovieDto{" +
                "name='" + name + '\'' +
                ", genre='" + genre + '\'' +
                '}'+super.toString();
    }
}
