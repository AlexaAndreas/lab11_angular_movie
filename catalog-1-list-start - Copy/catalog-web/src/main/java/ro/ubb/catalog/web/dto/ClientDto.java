package ro.ubb.catalog.web.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ClientDto extends BaseDto {
    private String name;
    private String cnp;

    @Override
    public String toString() {
        return "ClientDto{" +
                "cnp='" + cnp + '\'' +
                ", name='" + name + '\'' +
                '}' + super.toString();
    }
}
