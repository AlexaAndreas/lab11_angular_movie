package ro.ubb.catalog.web.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class RentDto {
    private Long clientId;
    private Long movieId;
    private String dueDate;
}
