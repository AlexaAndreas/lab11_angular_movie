package ro.ubb.catalog.web.converter;

import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.model.Movie;
import ro.ubb.catalog.core.model.Student;
import ro.ubb.catalog.web.dto.MovieDto;
import ro.ubb.catalog.web.dto.StudentDto;

@Component
public class MovieConverter extends AbstractConverterBaseEntityConverter<Movie, MovieDto>{

    @Override
    public Movie convertDtoToModel(MovieDto dto) {
        Movie movie = Movie.builder()
                .name(dto.getName())
                .genre(dto.getGenre())
                .build();
        movie.setId(dto.getId());
        return movie;
    }

    @Override
    public MovieDto convertModelToDto(Movie movie) {
       MovieDto dto = new MovieDto(movie.getName(),movie.getGenre());
       dto.setId(movie.getId());
       return dto;


    }
}
